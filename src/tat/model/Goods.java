package tat.model;

/**
 * Created by ilmir4400 on 11/25/14.
 */
public class Goods {

    private String nameGoods;
    private int numberGoods;
    private double priceGoods;

    public Goods(String nameGoods, int numberGoods, double priceGoods) {
        this.nameGoods = nameGoods;
        this.numberGoods = numberGoods;
        this.priceGoods = priceGoods;
    }

    public String getNameGoods() {
        return nameGoods;
    }

    public void setNameGoods(String nameGoods) {
        this.nameGoods = nameGoods;
    }

    public int getNumberGoods() {
        return numberGoods;
    }

    public void setNumberGoods(int numberGoods) {
        this.numberGoods = numberGoods;
    }

    public double getPriceGoods() {
        return priceGoods;
    }

    public void setPriceGoods(double priceGoods) {
        this.priceGoods = priceGoods;
    }

    @Override
    public String toString() {
        return
                "nameGoods='" + nameGoods + '\'' +
                ", numberGoods=" + numberGoods +
                ", priceGoods=" + priceGoods
                ;
    }
}

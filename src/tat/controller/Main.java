package tat.controller;

import tat.model.Goods;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ilmir4400 on 11/25/14.
 */
public class Main {


    public static Scanner in = new Scanner(System.in);
    public static  List<Goods> listIntegers = new ArrayList<Goods>();

    public static void main(String[] args) {

        System.out.println("=====================\nexit = 0\nadd = 1"+"\nread  = 2"+"\nupdate  = 3"+"\ndelete  = 4");
        int a=0;
        while(a!=1) {
            int number = in.nextInt();
            switch (number) {
                case 0:
                    System.out.println("exit");
                    a=1;
                    break;
                case 1:
                    System.out.println(" add");
                    add();
                    break;
                case 2:
                    System.out.println("read");
                    read();
                    break;
                case 3:
                    System.out.println("update");
                    System.out.print("Enter index");
                    int indexU = in.nextInt();
                    update(indexU);
                    break;
                case 4:
                    System.out.println("delete");
                    System.out.print("Enter index");
                    int indexD = in.nextInt();
                    delete(indexD);
                    break;
                default:
                    System.out.println("not number = " + number);

            }
        }


    }

    private static void delete(int indexD) {
        listIntegers.remove(indexD);
    }

    private static void update(int index) {
        System.out.print("Enter name goods ");
        String name = in.next();
        System.out.print("Enter number goods ");
        int number = in.nextInt();
        System.out.print("Enter price goods ");
        double price = in.nextDouble();
        listIntegers.set(index, new Goods(name, number,price));

    }

    private static void read() {

        for (Goods listInteger : listIntegers) {
            System.out.println(listInteger);

        }
    }


    static void add(){
        System.out.print("Enter name goods ");
        String name = in.next();
        System.out.print("Enter number goods ");
        int number = in.nextInt();
        System.out.print("Enter price goods ");
        double price = in.nextDouble();
        listIntegers.add(new Goods(name,number,price));

    }
}
